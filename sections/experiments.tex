
\renewcommand{\arraystretch}{1.5}
\begin{table*}[!t]
	\centering
	\caption{Description of the experimental scenarios (A*: single-node, B*: multi-node scenarios).}
	\begin{tabular}{c c c l l}
		\bfseries Label & \bfseries Nodes & \bfseries QoS Goal & \bfseries Status & \bfseries Degradation \\
		\hline
		A1      &  1      & 12K MC, 600s    & OK: under usage of the CPUs         & No    \\
		A2      &  1      & 14K MC, 600s    & OK: full usage of the CPUs          & No    \\
		A3      &  1      & 14K MC, 600s    & Not OK: single-node not enough      & Yes, 2 cores 2, 4, 6, 7 running at $performance < 100\%$      \\
		\hline
		B1      &  2      & 15K MC, 600s    & OK: two nodes for A.3 goal          & Yes, 2 cores 2, 4, 6, 7 (of node 1) at $performance < 100\%$  \\
		%		B2      &  16     & 80K MC, 600s    & OK: multiple nodes exploited        & Yes, 2 cores at $performance < 100\%$    \\
		B2      &  16     & 20K MC, 600s    & OK: concurrency addressed           & No \\
		\hline
	\end{tabular}
	\label{tab:scenarios}
\end{table*}

In this section we present the experimental evaluations, carried out considering different configurations 
of hardware setup and scenarios running multi-threaded applications from a scientific use-case.
We collected numerical observations about
performance, system power consumption and CPUs temperature, comparing the effects of managing the workload
execution through the BarbequeRTRM versus the ``unmanaged'' option.
By using the term "unmanaged" we mean that the resource allocation is in charge of the default Linux scheduler
(CFS) and management frameworks (e.g., cpufreq).
We used a server monitoring tool (Ganglia) to collect data about the temperatures and system power consumption.

Eventually, we close the section with some considerations concerning the performance of the PerDeTemp
policy, to provide a rough idea about the rate at which we can think about reconfiguring the system 
resources allocation.

\subsection{Experimental setup}

\subsubsection{Hardware}

For the experimental evaluations we used two hardware configurations provided by the IT4Innovations
National Supercomputing Center, in Ostrava (CZ).

The first configuration is a single-node HPC system consisting in a SuperMicro  X10DAI  blade hosting two
8-core Intel Xeon CPU E5-­2640-v3 at 2.60GHz clock rate, 256GB DDR3 memory at 1866MHz  without  ECC. The
operating system is a GNU/Linux distribution with kernel 4.2. We disabled the hyper-threading support of
the Intel CPUs to do not affect the experimental data with unpredictable behaviours. Therefore the system
include 16 cores in total.
 
The second configuration instead consists of a multi-node HPC system. More in detail, a 16 nodes chassis
from the ANSELM cluster \cite{Anselm2015}, where each node includes a pair of 8-core Intel Xeon CPU E5-­2640-v3
and 64GB of memory. The hyper-threading support has been disabled also in this case. All the nodes are
interconnected through InfiniBand.

%
%For the experiments evaluation we used a HPC infrastructure. The configuration consisted of a multi-node HPC
%system \cite{anselm2015}. The chassis consist of x86-64 Intel based nodes built on Bull Extreme Computing Bullx
%technology. The chassis is armed with 18 nodes (blades)  where each node includes a pair of 8-core Intel Xeon CPU
%E5-­2640-v3 and 64GB of memory. The hyper-threading support has been disabled in this case. All the nodes are
%interconnected by InfiniBand QDR network and by Gigabit Ethernet.


%{anselm2017} https://docs.it4i.cz/anselm/hardware-overview/ (Feb 2017)

\subsubsection{Application use-case}

The presented experiments use a model from the hydrological domain. First, it includes the
\emph{Rainfall/Run-off} model \cite{Golasowski2015} based on Monte Carlo Simulation of imprecisions in
the input data. It is a dynamic mathematical model, which transforms rainfall to flow at the catchment
outlet. It uses the SCS-CN method \cite{Beven2012} for converting rainfall to run-off with its two main
parameters: 1) the initial abstraction \textit{Ia} that defines the amount of water in the
soil at time zero; 2) the curve number \textit{CN} approximated from the hydrological soil group, land
use, and conditions of the catchments in the area.
This model has been implemented as a multi-threaded applications by using the OpenMP. A version using
Message Passing Interface (MPI) has been also used for executing the model on a distributed system
with multiple nodes.

\subsubsection{Scenarios}


We performed a set of tests to assess the validity of the proposed approach. We executed several scenarios, described in \tablename \ref{tab:scenarios}, distinguished into two classes: executions performed on the
single-node configuration (scenarios ``A'') and on the multi-node one (scenarios ``B'').
The scenarios are characterized by different application QoS goals, in terms of number of Monte-Carlo
samples to process in a given time frame (deadline).
On the hardware side, the CPUs may present a performance variable profile. As previously explained, this
comes from degradation conditions that we simulated with the usage of Bubble framework.


% ========================================== SINGLE NODE ========================================== %

\subsection{Single-node system configuration}

In this section  we show the results of experimental scenarios, from A1 to A3, previously
described in \tablename{\ref{tab:scenarios}. We used one instance of the \emph{Uncertainty} model, spawning
several threads, as workload. 
In the table we can observe the  details  concerning  the  performance  requirements of  the
application  for each  scenario,  i.e.,  the  number  of  Monte-­Carlo  samples  to  process  in  the
given  time  frame.  We compared our proposed approach to the default Linux OS policies, consisting
of the CFS scheduler with the cpufreq governor set to \emph{ondemand}. For this configuration we evaluated
if the proposed approach based on the BarbequeRTRM allows us to meet the application performance
requirements, what is the impact on the CPU per-core temperature and how the presence of degradation is
managed.


\subsubsection{Performance evaluation}

\begin{figure*}[!t]
\centering
\includegraphics[width=.9\textwidth]{figures/A1_B1_satisfaction.pdf} 
\caption{Performance satisfaction and number of threads spawned by the Uncertainty model in scenarios 
A1, A2, A2 (single-node) and B1 (dual-node).}
\label{fig:satisfaction}
\end{figure*}

\figurename \ref{fig:satisfaction} shows 1) the number of threads spawned by the application, according to
the number of CPU cores allocated by the BarbequeRTRM policy (PerDeTemp);
2) the performance satisfaction value.
Specifically, a value equal to 1 indicates that the current processing rate perfectly matches
the QoS level required,  i.e. to complete the  expected number of MC samples without missing the deadline. 
A satisfaction  value greater than 1 means that the application is “over-­performing”, i.e. is running
faster than what is needed to accomplish the required QoS level. Conversely, values lower than 1 represent
“under-­performing”  conditions.   

In scenario A1 (purple lines) we can observe how the number of cores assigned by the PerDeTemp  policy 
converges to 12 (over 16 in total), after a transient phase due to the learning process of the  policy.
After this learning phase, the application satisfaction also reaches a steady state with a value around 1.1.

Scenario A2 differs from A1 only for the higher performance requirements of Uncertainty. The number of MC
samples to process is now 14K. The idea behind this scenario is to approach the boundaries of the
performance that can be delivered by exploiting computing resources of a single node. As we can see by
observing the green lines in the figure, the PerDeTemp policy finds a stable satisfaction value equal to 1
by assigning an amount of CPU quota between 13 and 14 cores.

In  scenario  A3, the performance requirements of the Uncertainty model are the same defined for scenario
A2. The difference in this case comes from the underlying hardware status. Using the  Bubble  framework,
we simulated the presence of four CPU cores (2, 4, 6, 7) for which a certain percentage of performance
degradation  has  been  detected. On the application side, the effect of this simulation is that the
previous  resource  allocation  choices  carried  out  by  the  policy are no more suited to satisfy the 
application  performance  requirements.  In  fact,  PerDeTemp  must now
take into account this performance variability condition in its CPU cores mapping stage. It turns out
that in this case the policy assigns to the application all the CPU cores available  in  the  current  node.
However, the presence of degradation in the CPUs is such that the total amount resources  available 
in a single node is not enough to accomplish the performance goal. The resultant satisfaction is therefore
lower than 1, as we can see by observing the blue line in the figure. 

\begin{figure*}[t]
\centering
\subfloat[BarbequeRTRM+PerDeTemp]{
	\includegraphics[width=.45\textwidth]{figures/A3_CPU_loadmap.pdf}
	\label{sfig:load-bbque}
}
\hfill
\subfloat[Default Linux OS]{
	\includegraphics[width=.45\textwidth]{figures/A3_CPU_loadmap_nobbque.pdf}
	\label{sfig:load-nobbque}
}
\caption{CPU load map of the node. BarbequeRTRM managed with PerDeTemp policy versus unmanaged default Linux
configuration.}
\label{fig:loadmaps}
\end{figure*}

In \figurename \ref{fig:loadmaps} we can compare the CPU load maps in the two cases: the BarbequeRTRM
managed execution with the \emph{PerDeTemp} policy \figurename \ref{sfig:load-bbque} against the default
unmanaged Linux configuration \figurename \ref{sfig:load-nobbque}. While in the default Linux case the
CPU of the nodes are homogeneously allocated (load close to $100\%$), in the \emph{PerDeTemp} case the
resource manager allows us to consider the degradation affecting the CPU cores, such that the policy can
could be aware of the impossibility of achieved the expected performance. Moreover, if the degradation
is due to transient phenomenons due to full CPU load, we can decrease the utilization rate of such 
cores, in order to lately recover its complete operativeness. This behaviour should appear evident by 
comparing the two figures.


\subsubsection{CPU Temperature}

\begin{figure*}
\centering
\subfloat[A1 default Linux]{
	\includegraphics[width=0.5\textwidth]{figures/A1_thermal_map_nobbque_LEG.pdf} 
	\label{sfig:a1-temp-linux}
}
\subfloat[A1 BarbequeRTRM+PerDeTemp]{
	\includegraphics[width=0.5\textwidth]{figures/A1_thermal_map_LEG.pdf}
	\label{sfig:a1-temp-bbque}
}
\hfill
\subfloat[A2 default Linux]{
	\includegraphics[width=0.5\textwidth]{figures/A2_thermal_map_nobbque_LEG.pdf} 
	\label{sfig:a2-temp-linux}
}
\subfloat[A2 BarbequeRTRM+PerDeTemp]{
	\includegraphics[width=0.5\textwidth]{figures/A2_thermal_map_LEG.pdf} 
	\label{sfig:a2-temp-bbque}
}
\hfill
\subfloat[A3 default Linux]{
	\includegraphics[width=0.5\textwidth]{figures/A3_thermal_map_nobbque_LEG.pdf} 
		\label{sfig:a3-temp-linux}
}
\subfloat[A3 BarbequeRTRM+PerDeTemp]{
	\includegraphics[width=0.5\textwidth]{figures/A3_thermal_map_LEG.pdf} 
	\label{sfig:a3-temp-bbque}
}
\caption{Thermal maps of the CPU cores of one cluster node during the execution of scenarios A1, A2 and	A3.
	Comparison between the BarbequeRTRM managed case and the default Linux configuration, with cpufreq governor set to \emph{ondemand}.}
\label{fig:thermalmaps}
\end{figure*}

In \figurename \ref{fig:thermalmaps} we grouped the thermal maps of showing the temperatures of all the 16
CPU cores, reached during the execution of the Uncertainty model according to the three scenarios, 
comparing the default Linux configuration (\figurename \ref{sfig:a1-temp-linux}, \ref{sfig:a2-temp-linux}
and \ref{sfig:a3-temp-linux}) against the BarbequeRTRM managed cases (\figurename \ref{sfig:a1-temp-bbque},
\ref{sfig:a2-temp-bbque} and \ref{sfig:a3-temp-bbque}).

As described in \tablename \ref{tab:scenarios}, in scenario A1 the system is not fully utilized. The 16
threads spawned by the application in the default unmanaged configuration are migrated among the cores
according to the load balancing strategy of the Linux scheduler. As we can see in
\figurename{\ref{sfig:a1-temp-linux}} threads migration alone is not enough to avoid hot-spots. The
BarbequeRTRM case shown in \figurename{\ref{sfig:a1-temp-bbque}} looks more effective in this, with a
the peak temperature dropping from $70^\circ$C to $57^\circ$C.

For the A2 scenario (\figurename{\ref{sfig:a2-temp-linux}} and \figurename{\ref{sfig:a2-temp-bbque}}) we can
observe similar results, but since the resource requirements are higher, the peak temperature drop is lower.
In fact, in this case its value
drops from $70^\circ$C to $61.4^\circ$C. This happens because in the default Linux configuration all the CPU
cores are fully utilized for most of the time. This reduces the possibility of dissipating heat by migrating
threads to cores with a lower rate of utilization. Conversely, in the BarbequeRTRM case this mechanism
can be still exploited because the \emph{PerDeTemp} policy bound the assignment of CPU cores according to
the actual performance requirements of the Uncertainty model, as already explained. In this scenario the
application does not need a full utilization of all CPU cores available in the system to complete the
processing of 14K MC samples in 10 minutes.

In A3, due to the simulation of degradation in the default Linux case we observed a peak temperature of 
$68^\circ$C (\figurename{\ref{sfig:a2-temp-linux}}). The gap is closer with respect to the BarbequeRTRM managed
case (\figurename{\ref{sfig:a2-temp-bbque}}, for which we recorded a value of $63^\circ$C. The resource manager
reacts to the presence of degradation by reducing the utilization rate of the affected CPU cores. Accordingly,
although the application performance requirements would lead to the full utilization of all the cores, taken
into account this information we can still spread the heat in a more effective way with respect to the default
configuration.


\subsection{Multi-node system configuration}

This section includes the Uncertainty model execution evaluation in the multi-node configuration. By
using the Message Passing Interface (MPI) programming paradigm we spawned processes over the entire cluster
(16 nodes), comparing the BarbequeRTRM managed execution to the default Linux configuration in terms of
performance satisfaction, CPUs thermal profiles and overall system power consumption. We considered the
scenarios B1 and B2 described in \tablename{\ref{tab:scenarios}}.

\subsubsection{Performance evaluation}


\begin{figure*}[!t]
	\centering
	\includegraphics[width=0.9\textwidth]{figures/B3_satisfaction.pdf} 
	\caption{Performance satisfaction and number of threads spawned by the Uncertainty model in scenario
		B2 (16 nodes) in the BarbequeRTRM managed execution.}
	\label{fig:satisfaction-multi}
\end{figure*}

In scenario B1, the performance requirements of the Uncertainty model are the same reported for A3. What we aim
at showing in this case is the effect of BarbequeRTRM when we need to scale the execution of
application over multiple nodes. Scenario A3 has indeed put in evidence a condition for which, due to the
presence of CPU affected by degradation, the performance delivered by that single node of the entire HPC
infrastructure are no more enough (\figurename \ref{fig:satisfaction}, blue line). Therefore, we need to
get access to computing resources of further nodes. This is achieved thanks to the MPI implementation of the
model. In this regard, it is worth to explain how MPI and the proposed application execution model work together.
What happens is that we launch the application by spawning one MPI process per nodes. This means that we
have an Uncertainty model instance per node, each processing a subset of input data. The idea is that we 
can consolidate the overall system resources utilization by keeping “disabled” (or “idling”) each instance
unless we really need it, according to the current performance requirements. A "master" instance is responsible
of waking up some "slave" instances specifying the performance goal to meet. Accordingly, each application
instance interact with the local BarbequeRTRM asking for resources, until the expected performance level is
reached. 

In scenario B1, the master instance of Uncertainty runs on node 0. Since the satisfaction level is below 1 and
the resource manager cannot do anything more, the master wakes up the slave instance deployed onto node 1. The
overall application performance requirements are therefore split between two running instances. The results
visible in \figurename \ref{fig:satisfaction} (orange line) show how the performance goal is met (value $>= 1$)
although both nodes are affected by degradation.

Scenario B2 instead is characterized by the fact that, in order to achieve the required application
performance, we need to exploit the entire cluster, i.e., to enable the execution of all the model instances. 
What we expect from testing the execution of such scenarios is the scaling of benefits experienced for the
single-node case over multiple nodes. More specifically, in this scenario we aimed at observing how the
BarbequeRTRM, with the PerDeTemp policy, is capable of reacting to performance variability conditions caused by
the concurrent execution of different applications on the same node, considering a system configuration of 16
nodes.
To this purpose, two separate instances of the Uncertainty model have been launched, both spawning a process on
each node, with the goal of terminating the execution approximately at the same time. The first instance is
launched at time t=0 and is required to process 20K MC samples in 10 minutes ($600s$). The second instance
is launched $50s$ after ($t=50s$) with the goal of processing 27.5K samples. 
The \figurename{\ref{fig:satisfaction-multi}}, shows how the two instances adapt the number of active threads (jobs) 
to the number of CPU cores assigned by BarbequeRTRM, which in turn is driven by the current performance level required
to do not miss the deadline. At the beginning, the first application instance is served with all the CPU cores
available in the cluster. While the policy infers that the
application is “over-performing” the amount of cores assigned is progressively reduced. The start of a second
instance introduces concurrency, leading the BarbequeRTRM to rapidly increase the number of cores assigned to
the first instance. After few seconds, the oscillations of number of cores assigned to each instance, and thus
the number of active threads, is stabilized around 50 in total. Looking at the satisfaction graph below, what is
worth to highlight is how this adaptive execution keeps the satisfaction level equal to 1 and, most important,
allows both the Uncertainty instances to complete their execution in the 10 minutes required time frame.



\subsubsection{CPU Temperature}

Concerning the impact on the CPUs temperature, in \figurename{\ref{fig:thermalmaps-multi}}, we can compare the
BarbequeRTRM approach to the default Linux configuration during the execution of scenario B1. The images show
the thermal maps of the CPU core of the two nodes used to run the scenario. Observing the
temperature of cores from both nodes, it is immediate to see how in the BarbequeRTRM managed case
(\figurename{\ref{sfig:b1-temp-bbque}}) we do not have any hot-spot with respect to the unmanaged case
(\figurename{\ref{sfig:b1-temp-linux}}) and the peak temperature value falls to $56^\circ$C. This thanks also
to the possibility of spreading the request of resources over two nodes.

In \figurename{\ref{sfig:b2-temp-linux}} and \figurename{\ref{sfig:b2-temp-bbque}} we can respectively
observe the thermal maps built by considering the peak temperature of the CPUs sampled from each node of the
16-node cluster. The figure shows the execution of scenario B2, where the two Uncertainty models execute
concurrently over the entire system.
Once again we can see how in the BarbequeRTRM managed case the temperature distribution is much more levelled with
respect to the unmanaged cases. Looking at the difference of peak temperature observed over all the cores
of all the nodes, we see a decrease from $71^\circ$C to $67.4^\circ$C.

We discuss how the temperature affect the system reliability, in \ref{ssec:reliability}. 

\begin{figure*}[!t]
	\centering
	\subfloat[B1 default Linux]{
		\includegraphics[width=0.48\textwidth]{figures/B1_thermal_map_nobbque.pdf} 
		\label{sfig:b1-temp-linux}
	}
	\hfill
	\subfloat[B1 BarbequeRTRM+PerDeTemp]{
		\includegraphics[width=0.48\textwidth]{figures/B1_thermal_map.pdf}
		\label{sfig:b1-temp-bbque}
	}
	\hfill
	\subfloat[B2 default Linux]{
		\includegraphics[width=0.48\textwidth]{figures/B2_thermal_map_nobbque.pdf} 
		\label{sfig:b2-temp-linux}
	}
	\hfill
	\subfloat[B2 BarbequeRTRM+PerDeTemp]{
		\includegraphics[width=0.48\textwidth]{figures/B2_thermal_map.pdf}
		\label{sfig:b2-temp-bbque}
	}
	\caption{Thermal maps of the CPU cores of two cluster node during the execution of scenario B1 and B2.
		Comparison between the BarbequeRTRM managed case and the default Linux configuration, with cpufreq governor set to \emph{ondemand}.}
	\label{fig:thermalmaps-multi}
\end{figure*}


\subsubsection{System power consumption}

To evaluate the overall system power consumption we ran the Uncertainty model according to two performance goals:
20K and 80K MC samples in 10 minutes. %8 and 6 minutes, respectively. tonipat@20170217 to be honest the
%application does no scale very well, the results are for 10 minutes asfaik...

In the former case, the utilization of the cluster is not
full, neither in the unmanaged nor in the BarbequeRTRM managed case. What is immediate to catch in
\figurename{\ref{sfig:syspower-20K}} (blue line), is a ``spike'' of 1700W in case of default Linux configuration,
which rapidly raises up the temperature of the CPUs, triggering thermal cycles that accelerate the ageing process
of the silicon. As we previously said, this increase the probability of experiencing faults and generate
variability in the performance delivered by the different cores.
In the BarbequeRTRM managed case (\figurename{\ref{sfig:syspower-20K}}), the PerDeTemp policy bounds the CPU
allocation as described yet in Section \ref{sec:rw}. A further consequence of this approach is a smoother
variation of power consumption (and thus CPUs temperature), as we can see by observing the red line in the figure.
After the initial learning phase of the PerDeTemp policy, where the power reaches a peak value around 900W ($47\%$
less with respect to unmanaged execution), the overall consumption is then stabilized around 800W, which is less
than half the peak in the blue line. It is worth to remark that this value includes both the power supply required
by the system and the cooling infrastructure (fans).

Increasing the performance requirements as described for the second configuration, we observed the full utilization
of the cluster. Once again, this happened for the execution in the unmanaged default Linux configuration. The
power consumption has reached 2400W, while by using our approach most of the execution has required power for
1500W on average, which means $37\%$ less.

These results can be translated into a two-fold benefit. In fact, with the proposed approach along with saving
power we can also extend the lifetime of the system and improve the reliability of its components. Regarding this
point we introduced some quantitative considerations in the next Section \ref{ssec:reliability}.

%Here we see the power consumption of the 16th nodes where the Uncertainty model instance runs. There are two sub-figures a) presents the computation of 20K samples, meanwhile b) presents the computation for 80K samples in ten minutes.  Considering the 20K samples case, the peak power consumption of the node in the unmanaged configuration is around 140 W with respect to the 50-60 W of the BarbequeRTRM managed configuration. This means that with BarbequeRTRM we achieved a power consumption reduction of 80-90 W per node, i.e., 60\% in average. In the 80K samples case, the load per node is clearly higher with respect to the 20K samples processing goal. The drop of peak power consumption observed now goes from about 150 W to 100 W (worst case), which means a reduction of 33\%.  Looking at the area under the set of power consumption curves, we can easily understand how the price to obtain this result has been paid in terms of energy consumption, which is higher in the BarbequeRTRM case, especially when the performance goal does not require a full usage of the CPUs. We consider the possibility of overcoming this drawback by improving the Perdetemp policy in the future, or alternatively develop a new HPC-oriented policy to introduce in the BarbequeRTRM framework.


\begin{figure*}[!t]
	\centering
	\subfloat[20K samples]{
		\includegraphics[width=0.48\textwidth]{figures/ondemand_20000_power_total.pdf} 
		\label{sfig:syspower-20K}
	}
	\hfill
	\subfloat[80K samples]{
		\includegraphics[width=0.48\textwidth]{figures/ondemand_80000_power_total.pdf}
		\label{sfig:syspower-80K}
	}
\caption{Overall cluster power consumption (16 nodes) during the execution of the Uncertainty model,
	according to two different performance goals. Comparison between the BarbequeRTRM managed case (red line)
	and the default Linux configuration, with cpufreq governor set to \emph{ondemand}.}
\label{fig:syspower}
\end{figure*}

\subsection{Reliability}
\label{ssec:reliability}

The increase of CPU temperature, observed during the execution of the experimental scenarios, generate a stress
condition over the devices that can accelerate the process of ageing and increase of failure probability. In this
section we present the estimation of the impact on system reliability of our resource management strategy.
To this purpose we consider the Mean Time Between Failure (MTBF), a well-known reliability metric, expressing the average time it takes for a failure to occur in a electronic device. The value of this metric can be
estimated by using the following expression:
\begin{equation}
MTBF = \frac{device\_hours}{number\_of\_failures}
\end{equation}

We can also define the \emph{Failure Rate (FR)} as the reciprocal of the MTBF:
\begin{equation}
FR = 1 / MTBF
\end{equation}

However, since collecting enough data about failures would require so much time (typically years), we decided to rely on a model in order to get an estimation of the impact on the MTBF of the relatively short-time executions
of our experimental workload.

In this regard, suppose a device is stressed being subject to a high temperature $T_2$ for a time $t_{2}$.
We need to find the equivalent $t_{1}$ , which would cause the device the same level of stress at a lower temperature $T_{1}$.
To this purpose, noted that if $T_2 > T_1 $ then $t_2 < t_1$, we need to introduce the \emph{Acceleration
Factor} defined as follows:
\begin{equation}
\label{eq:accfactor}
 A = t_{1} / t_{2}
\end{equation}

Therefore, given the failure rate $FR_{1}$ for the device subject to temperature $T_{1}$ for the time $t_{1}$,
the failure rate $FR_{2}$ for the device subject to temperature $T_{2}$ for the time $t_{1}$ can be computed as:
\begin{equation}
\label{eq:failurerate}
 FR_{1} = A * FR_{2}
\end{equation}

This means that for estimating the failure rate $FR_{1}$ of a computing system running a workload for a long
time frame $t_{1}$, given a shorter execution time $t_{2}$, we need to find such acceleration factor.
Concerning this point, the \emph{High Temperature Bias (HTRB)} test is an example of a temperature-accelerated experimental approaches. It is usually found that the rates of the reactions causing device failure are
accelerated with temperature according to the Arrhenius model\cite{arrenhius}:

\begin{equation}
\label{eq:arrehenius}
A = e^{\frac{E_A}{K}[\frac{1}{T_{1}}-\frac{1}{T_{2}}]}
\end{equation}

where $E_{A}$ is the \emph{activation energy} of the failure mechanism and $k$ is the Boltzmann constant ($8.6*10^{-5} eV/K$).
The activation energy, EA, is found experimentally and it is usually of the order of $1.0eV$, depending on
the predominant failure mechanism. For silicon devices, such values is typically in the $[0.3-0.9]eV$ range.

\subsubsection{Mean Time Between Failures (MTBF) Estimation}

Given the model previously introduced, we estimated the MTBF for the scenario A2 (Uncertainty model processing
14K samples in $600s$). Recalling that the system includes two 8 cores CPUs, we took the minimum
temperature $T_1$ and maximum temperature $T_2$ of each CPU, comparing the execution with our run-time
management approach (\emph{PerDeTemp}) against the unmanaged (\emph{Default}) strategy.
The temperature values are show in \tablename \ref{tab:kelvin}. 

Please note that the position of the fan on one side of the blade produces a difference of around 10 degrees
between the two CPU sockets.

\begin{table}
\caption{Minimum and maximum temperatures for each CPU, during the execution of scenario A2 comparing the PerDeTemp resource allocation
policy with respect to the unmanaged "best-effort" approach.}
\label{tab:kelvin}
\centering
\begin{tabular}{c|cc|cc}
      & \multicolumn{2}{c}{\textbf{CPU 0}}  & \multicolumn{2}{c}{\textbf{CPU 1}}  \\
      & min ($^\circ$K) & max ($^\circ$K) & min ($^\circ$K) & max ($^\circ$K)     \\
\hline
Default     &  328.4 & 340.5   & 313.0 & 328.4 \\
PerDeTemp   &  321.8 & 327.5   & 310.8 & 318.7 \\
\hline
\end{tabular}
\end{table}

\begin{table}
\caption{Minimum and maximum temperatures for each CPU, during the execution of scenario A2.}
\label{tab:mtbf}
\centering
\begin{tabular}{c|cc|cc}
      & \multicolumn{2}{c}{\textbf{CPU 0}}  & \multicolumn{2}{c}{\textbf{CPU 1}}  \\
      & 0.3 ($eV$) & 0.9 ($eV$) & 0.3 ($eV$) & 0.9 ($eV$)     \\
\hline
Default     &  0.69 & 0.32   & 0.67 & 0.31 \\
PerDeTemp   &  0.83 & 0.56   & 0.76 & 0.44 \\
\hline
\end{tabular}
\end{table}

According to Equation \ref{eq:arrehenius}, we used values 0,3eV and 0,9eV for $E_{A}$, considering that, as we said, 
the real value falls in this range depending on the specific silicon technology. We estimated the
acceleration factor A, and then relative MTBF for the two CPUs, comparing the two resource management
approaches. The results summarized in \tablename{\ref{tab:mtbf}} show how with the PerDeTemp policy we improved the
reliability from 17\% to 43\% for the CPU under the worst cooling condition, and from 11\% to 30\% for the CPU
under the best cooling conditions.


% ========================================== PerDeTemp ========================================== %

\subsection{PerDeTemp policy performance}


In this final subsection about experimental results we briefly report some considerations concerning the performance of the proposed PerDeTemp. We assumed to have the following input: a system including a number
of CPU cores spanning from 4 to 32 and from 1 to 32 active applications to schedule. The policy execution
time is shown in  \figurename \ref{fig:policy-ovh}.
Although it grows linearly with the input size, we can observe how in a realistic case of a computing
node with 32 cores under a "heavy" load condition (32 applications) the policy completes the resource allocation in less than $10$ms.

To this time we should add the overhead required by the Linux \texttt{cgroup} framework to enforce the
policy output. According to the BarbequeRTRM developers this is usually in the order of tens of milliseconds
($60-70$ms in the worst case above). A further overhead may also be represented by the time spent in
the \texttt{onConfigure} function, which a very application specific contribution. Assuming that this
amount of time could be equal to a few milliseconds, we can state that proposed approach allows us
to potentially redefine the overall resource allocation several times per second.

\begin{figure*}[!t]
	\centering
	\includegraphics[width=\textwidth]{figures/perdetemp_overhead.pdf}
	\caption{The PerDeTemp policy execution time given a variable number of active applications and
		available CPU cores number as input.}
	\label{fig:policy-ovh}
\end{figure*}
