
\subsubsection{The Barbeque Run-time Resource Manager}

\begin{figure}
	\centering
	\includegraphics[width=0.85\columnwidth]{figures/bbque_aem.pdf}
	\caption{The run-time manageable execution model defined in the application library provided by the
	BarbequeRTRM.}
	\label{fig:bbque-aem}
\end{figure}


In this section we briefly recall the working principles of the \emph{BarbequeRTRM}. This
framework, which has been introduced in \cite{bellasi2015effective} , represents a run-time resource
management solution based on the idea of supporting the reconfigurable (or adaptive) execution
of the applications \cite{bellasi2012rtrm}. The system workload is generally not known a priori.
Similar approaches have been proposed also in \cite{7161531, 6560761, 4629252} under the definition of
\emph{malleable applications}.

Specifically, the BarbequeRTRM provides an \emph{Application Runtime Library} which must be linked to
the applications aiming at being run-time manageable.
The library provides a model to synchronize the execution of the application with resource allocation
policy outcome coming from the BarbequeRTRM side. This execution model is depicted in 
\figurename{\ref{fig:bbque-aem}}. On the left side, the application must implement a set of functions
defined in the library. These functions are invoked at run-time by a hidden control thread according
to the current execution stage of the application and the necessity of intervention of the resource
manager. The semantic behind the library functions implementation is the following:
\begin{itemize}
\item \texttt{onSetup}: application initialization, i.e., variables, structures, thread-pools, 
	initial performance goal;
\item \texttt{onConfigure}: configuration of the application parameters (e.g., number of active threads,
	iterations,\dots) according to the amount of assigned resources;
\item \texttt{onRun}: single cycle of computation (e.g., computing a single frame during a video encoding);
\item \texttt{onMonitor}: check of the current performance with respect to the goal;
\item \texttt{onRelease}: clean-up code, memory release, object destruction,\dots
\end{itemize}

Therefore, when the application starts it almost immediately enters the \texttt{onSetup}. Here
it can call the \texttt{SetCPSGoal()} function to set the initial performance goal in terms of throughput,
i.e. cycles-per-second (CPS). On the resource manager side, the policy determines a first set of assigned
resources (e.g.,CPU and memory) and bound the application in such a set, exploiting the Linux \emph{cgroup}
framework \cite{}. Then the \texttt{onConfigure} is called, where in the application can
check for example the amount of CPU quota (or number of cores) assigned through the \texttt{GetAssignedResources()}
function. This allows it to tune the number of active threads accordingly, minimizing potential
context-switch overheads and cache memory contention.

Once the configuration steps have been completed, the control thread calls the actual entry point
to start the input data processing: the \texttt{onRun} function. What we expect to be find in
this function is the code needed to complete the processing of part of the dataset, i.e. a frame
(or a set of). 
After each \texttt{onRun} execution, the next step is represented by the \texttt{onMonitor}.
The application developer can optionally exploit this function to check the current performance through
\texttt{GetCPS()}, or redefine the goal.
The executions goes on in a loop of \texttt{onRun} and \texttt{onMonitor}
invocations. The application library relies on this loop to collect statistics and build the
aforementioned \emph{run-time profiles} that are sent to the resource manager every $n$ cycles, where
$n$ is a configurable parameter. The BarbequeRTRM can thus compare the current performance to
the goal specified and evaluate the resources usage profile. This can trigger a new execution of
the allocation policy and accordingly the assignment of a different resource set.
In such a case the application re-enter the \texttt{onConfigure} function and reconfigure
itself before continuing with the input data processing.



\subsubsection{PerDeTemp resource allocation policy}

\begin{figure}[!t]
	\includegraphics[width=\columnwidth]{perdetemp_inputs}
	\caption{During runtime, the information coming from the on-chip thermal sensors
		and the	Bubble framework are used to create a thermal and a degradation
		map for the available CPU cores. The feedbacks coming from the Runtime Library
		are instead used to create a run-time Profile for running applications.}
	\label{fig:perdetemp_input}
\end{figure}

%When executing a multi-threaded application on a homogeneous multi-core CPU,
%we expect each core to be as performing as its siblings. Unfortunately, this
%is not always true: each core may be subject to a different degree of performance
%degradation due to temperature, aging and possibly faults.
As we introduce at the begin of this section, by interfacing the BarbequeRTRM 
with the Reliability monitor, we can actually be aware the performance-variability
and degradation affecting the underlying resources. This information is useful to
implement policies aiming at meeting the application performance requirements, 
while mitigating the ageing effects.

Given that the GNU/Linux CPU frequency governors do not handle this information,
we developed a resource allocation policy that tackles the issue.
The policy proposed in this section focuses on three objectives:

\begin{itemize}
	\item Making each application comply with its performance requirements;
	\item Minimizing the effect of degradation on the applications performance;
	\item Mitigating the hardware ageing process by levelling the temperature
		  distribution over the whole chip.
\end{itemize}

Given these objectives, we called the policy \textit{PerDeTemp} (PERformance,
DEgradation, TEMPerature).

The main idea beneath \textit{PerDeTemp} is schedule the managed applications
by allocating the minimum amount of CPU cores that allows them to comply with their
performance requirements. As to mapping, the set of cores that is allocated
to each application is selected so that the cores feature a minimum or at least
homogeneous degradation. During the process of selection, cold cores are
preferred to hot ones, and the resource allocation is periodically re-computed
to level the heat over the whole chip.

To allocate resources in a performance, degradation and temperature-aware
fashion, \textit{PerDeTemp} needs information about the status of both applications
(current performance and current resource usage) and hardware (current degradation
and temperature of each core). As shown in Figure \ref{fig:perdetemp_input},
this is possible due to the BarbequeRTRM, which gathers this information from multiple sources:

\begin{itemize}
	\item the Application Runtime Library automatically collect the profiling
	information and notifies about the BarbequeRTRM about current CPU usage and
	current performance;
	
	\item the Reliability monitor periodically sends to the BarbequeRTRM information
	about the about presence and entity of degradation of hardware resources;
	
	\item the on-chip sensors are used by the monitoring layers of the BarbequeRTRM
	to periodically	retrieve the temperature of each core.
\end{itemize}



Figure \ref{fig:perdetemp_output} shows the execution flow of the PerDeTemp allocation
policy. The inputs are the thermal and degradation maps, which are
used to compute a thermal and degradation-wise ranking of the CPU cores; and the
applications run-time profiles, which are used by the policy to compute an optimal
CPU quota allocation for each application. Ranking and CPU allocation information
are then used to compute an ideal resource mapping for the running applications.

The concept behind CPU quota allocation is straightforward: for example,
an octa-core processor offers $800$ms of total CPU time over a period of
$100$ms; allocating half of the total CPU time to an application would mean
allocating it $400$ms of CPU time over $100$ms, i.e. $400\%$ of \emph{CPU quota}.
To compute the optimal CPU quota for an application, \textit{PerDeTemp}
uses its measured CPU usage and the distance between current and desired performance.

Knowing the real CPU usage of applications allows \textit{PerDeTemp} to notice if
an application is not able to fully exploit the allocated CPU quota,
e.g. due to memory-boundedness, heavy synchronization or lack of parallelism.
In that case, the policy can seize back the unused quota and either assign it to other
applications, or consolidate the workload into less cores.

\begin{figure}[!t]
	\includegraphics[width=\columnwidth]{perdetemp_outputs}
	\caption{PerDeTemp execution flow: the thermal and degradation maps are used
		to compute a ranking for the CPU cores. The applications Runtime Profiles are
		instead used, along with the history of previous allocations, to compute an
		optimal CPU usage allocation for the applications. Raking and allocation data
		are then used to map applications on CPU cores.}
	\label{fig:perdetemp_output}
\end{figure}


The distance between current and desired performance is also crucial to compute
the CPU quota requirement of an application. To better understand the mathematical
meaning of such distance, let us first define the throughput of an application as
the number of \emph{onRun()} methods it executes in one second.
We call this number \emph{Cycles Per Second} (CPS). When an application declares
a CPS goal, the Runtime Library starts computing the percent distance between the
real and goal CPS. We call \emph{CPS gap} such a distance and define it as:

\begin{equation}
\label{eq:gap}
cps_{gap} = \frac{CPS_{curr}}{CPS_{goal}}-1
\end{equation}
\noindent
where {\scriptsize$CPS_{curr}$} and {\scriptsize$CPS_{goal}$} are the current and goal CPS,
respectively. For example, if an application requires a throughput of $20$ CPS and
is executing at $18$ CPS, its CPS gap equals to {\scriptsize$\frac{18}{20}-1 = -0.1$},
i.e., it is $10\%$ too slow.

To address performance variability, we generalize the formula by defining a minimum
and maximum required performance value. For example, the application can require a
throughput of $20 \pm 5\%$ CPS, i.e., the Runtime Library will not forward run-time profiles
to the resource manager while the average CPS is in the interval {\scriptsize$[19.0$ - $21.0]$}.
The new equation, which is the same of Equation \ref{eq:gap} when minimum CPS goal is
equal to maximum one, is:

\begin{equation}
\label{eq:pg}
cps_{gap} = \begin{cases}\frac{CPS_{curr}}{CPS^M_{goal}}-1 & \mbox{if } CPS_{curr} \geq CPS^M_{goal}\\
\frac{CPS_{curr}}{CPS^{m}_{goal}}-1 & \mbox{if } CPS_{curr} \leq CPS^m_{goal} \\
0 & \mbox{else} \end{cases}
\end{equation}
\noindent
where {\scriptsize$CPS^m_{req}$} and {\scriptsize$CPS^M_{req}$} are minimum and maximum
required performance, respectively.
Equation \ref{eq:pg} can be reused to define a gap for CPU usage:

\begin{equation}
cpu_{gap} = \begin{cases}\frac{C_{curr}}{C^M_{goal}}-1 & \mbox{if } C_{curr} \geq C^M_{goal}\\
\frac{C_{curr}}{C^{m}_{goal}}-1 & \mbox{if } C_{curr} \leq C^m_{goal} \\
0 & \mbox{else} \end{cases}
\end{equation}

\noindent
where {\scriptsize$C^m_{goal}$} and {\scriptsize$C^M_{goal}$} are the CPU allocations
that would allow performance to stay in the interval  {\scriptsize$[CPS^m_{goal} - CPS^M_{goal}]$}.
The scheduling policy objective is to translate a performance range
{\scriptsize$[CPS^m_{goal} - CPS^M_{goal}]$} into a CPU usage interval
{\scriptsize$[C^m_{goal} - C^M_{goal}]$}. Doing this means translating
CPS gaps into CPU gaps: if an application complains about its
current performance, the policy must understand how to adjust the current
(real) CPU usage to solve the issue.

By construction, each \emph{onRun()} executes the same code on different data,
and averaging the CPS mitigates the effects on data variability on performance.
Therefore, we decided to assume a linear relationship between CPS and measured
CPU usage. The resulting formula is:

\begin{equation}
cpu_{gap} \sim \begin{cases}\frac{CPS_{curr}}{CPS^M_{goal}}-1, & \mbox{if } CPS_{curr} \geq CPS^M_{goal}\\
\frac{CPS_{curr}}{CPS^{m}_{goal}}-1, & \mbox{if } CPS_{curr} \leq CPS^m_{goal} \\
0, & else \end{cases}
\end{equation}

\noindent
which leads to:

\begin{subequations}
	\label{eq:bideal}
	\begin{align}
	C^m_{ideal} & \sim \frac{C_{curr}}{(1 + cps_{gap})} & \quad\mbox{if } cps_{goal} < 0 \\
	C^M_{ideal} & \sim \frac{C_{curr}}{(1 + cps_{gap})} & \quad\mbox{if } cps_{goal} \geq 0
	\end{align}
\end{subequations}

This means that, given a CPS gap, we can estimate the CPU allocation, being it
the minimum or the maximum one depending on the sign of the CPS goal. Being
applications able to send feedbacks to the resource manager, allocations will
be continuously refined until an ideal allocation is found, thus eliminating
the inaccuracies induced by the linearity assumption.

The CPU allocation algorithm used in \textit{PerDeTemp} is shown in Listing \ref{listing:steaks}.
Depending on the resource manager configuration, the algorithm can be activated either
periodically or each time the resource manager receives an updated Runtime Profile.
Regardless of the configuration, \textit{PerDeTemp} is indeed activated each time
a new application starts.

When activated, the policy first serves known applications, i.e. those that are already
executing, in a priority-wise order
(Lines §\ref{listing:policy_known_start}§-§\ref{listing:policy_known_stop}§).
Applications that are satisfied with their current performance have their allocation
confirmed (line §\ref{listing:policy_confirmalloc}§); conversely, applications that are not
satisfied receive an updated CPU quota allocation, which is equal to their ideal CPU
quota (line §\ref{listing:policy_update_alloc}§) as computed in Equation \ref{eq:bideal}.
Please note that, even if the CPU quota allocation of an application is confirmed, the
mapping of such quota on the hardware may be modified depending on the current degradation
and thermal status of the system.

After having computed resource allocations for a given priority level
(line §\ref{listing:policy_known_stop}§), the policy checks if
the available resources are enough to satisfy the scheduling choice; if not, allocations
are normalized so that each application of that priority is equally served.

Finally, the policy serves unknown applications, i.e. the applications that have
just started (Lines §\ref{listing:policy_unknown_start}§-§\ref{listing:policy_unknown_stop}§),
assigning them a fair quantity of bandwidth. That is, after all the known
applications have been served and only if there are enough free resources,
the unallocated CPU quota is fairly divided, again priority-wise,
among the unknown applications.

\lstset{
	emph={foreach, in},
	emphstyle=\textbf,
	morecomment=[l]{//},
	commentstyle=\color{Green}\ttfamily,
	xleftmargin=3.5ex,
	escapechar=§,
	basicstyle=\scriptsize\ttfamily,   % the size of the fonts used for the code
	numbers=left,               % where to put the line-numbers
	numberstyle=\footnotesize,  % size of the fonts used for the line-numbers
	stepnumber=1,
	tabsize=3
}

\begin{lstlisting}[label={listing:steaks},
caption={CPU quota allocation in the PerDeTemp scheduling policy}]
foreach priority p: §\label{listing:policy_known_start}§
	// App is known if it already received an allocation
	foreach application a in KnownApplications(p):
		if ! HasNewRuntimeProfile(a):
			MapPreviousAllocation(a) §\label{listing:policy_confirmalloc}§
		else
			cpu_ideal = IdealCPUQuota(a.rt_profile)
			MapNewAllocation(a, cpu_ideal) §\label{listing:policy_update_alloc}§

	// Normalize allocations to total available
	// CPU quota (if needed)
	FinalizeAllocation(KnownApplications(p)) §\label{listing:policy_known_stop}§

foreach priority p: §\label{listing:policy_unknown_start}§
	// Available bandwidth given previous allocations
	available_cpu_q = GetAvailableCPUQuota()
	// If there are not enough resources to serve
	// them, low priority unknown applications will
	// be started later
	if available_cpu_q == 0: break

	// Number of unknown apps at this prio
	apps_n = UnknownApplications(p).size()
	if apps_n == 0: continue

	// App is unknown if it just started
	foreach application a in UnknownApplications(p):
		// Fair allocation: free resources divided by the
		// number of unknown applications at this prio
		MapNewAllocation(a, available_cpu_q / apps_n) §\label{listing:policy_unknown_stop}§
\end{lstlisting}